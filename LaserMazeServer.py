#!/usr/bin/env python3 

from flask import Flask, flash, render_template, send_from_directory, request, url_for, redirect
from flask_socketio import SocketIO, emit, join_room
import logging
import paho.mqtt.client as mqtt
import string

from SensorsManager import SensorsManager

app      = Flask('LaserMazeServer', template_folder='web', 
                    static_url_path='', static_folder="web/static")
app.config['SECRET_KEY'] = 'secret!_qnf87t1daqf9rk487abr8duw396dfmb7394qe59'
socketio = SocketIO(app)

VERSION     = '0.5'
MQTT_BROKER = "10.0.0.12"

@app.context_processor
def jinja_global_variables():
    """This function returns variables which will be available
    to every jinja2 template."""
    return dict(version=VERSION)


@app.after_request
def add_header(r):
    """Adds headers to every transaction to invaliadate cache 
    so HTML loads from server every time."""
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.route('/js/<path:path>')
def send_js(path):
    """Serves JS files from /web/js/ folder."""
    return send_from_directory('web/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    """Serves CSS files from /web/css/ folder."""
    return send_from_directory('web/css', path)


@app.route('/')
def index():
    """Serves home page (index)"""
    trippedTimes = app.sensorsManager.getTrippedTimes()
    gameRunning = app.sensorsManager.getGameState()
    return render_template('index.html', tripped_count=trippedTimes, game_running=str(gameRunning))


@app.route('/sensors')
def sensors():
    """Serves sensors config page"""
    sensorsData = app.sensorsManager.getSensorsData()
    return render_template('sensors.html', sensorsData=sensorsData)


@app.route('/sensors/add/', methods=['POST'])
def add_sensor_post():
    box_id = str(request.form['sensor_box_id'])
    allowed_chars = set(string.ascii_letters+string.digits+"_- ")
    if set(box_id).issubset(allowed_chars):
        return add_sensor(box_id)
    else:
        flash('Sensor box identificator contains invalid letters. Valid letters are letters, digits, underscore, dash and space.', "error")
        return add_sensor_html(box_id)
    
    
@app.route('/sensors/add/')
def add_sensor_html(box_id=""):
    return render_template('add_sensor.html', box_id=box_id)


@app.route('/sensors/add/<string:box_id>')
def add_sensor(box_id):
    app.sensorsManager.addSensor(box_id)
    return redirect(url_for('sensors'))


@app.route('/mqtt')
def mqtt_log():
    """Servers MQTT messages log."""
    return render_template('mqtt_log.html')


@socketio.on('connection')
def handle_connection(data):
    """Reacts on SocketIO connection request"""
    print('received connection json:', str(data))
        
    if(data["status"] == "connect"):
        if(data["type"] == "mqtt_log"):
            print("client joined mqtt_log")
            join_room('mqttlog')
        elif(data["type"] == "game"):
            print("client joined game")
            join_room('game')
        else:
            print("unknown type ("+str(data["type"])+") of connection reequest")


@socketio.on('sensorSettings')
def handle_sensorSettings(json):
    print('received sensorSettings: ' + str(json))
    if( json["action"] == "activate" ):
        app.sensorsManager.sensorBox(json["sensorBox"]).enable(json["sensor"])
    elif(json["action"] == "deactivate"):
        app.sensorsManager.sensorBox(json["sensorBox"]).disable(json["sensor"])
    elif(json["action"] == "set"):
        #data.setTime( int(json["mm"])*60+int(json["ss"]) )
        pass
    socketio.emit("sensorSettings", json, room="game")


@socketio.on('gameSettings')
def handle_gameSettings(json):
    print('received gameSettings: ' + str(json))
    if(json["action"] == "resetTrippedCounter"):
        app.sensorsManager.resetTrippedCounter()
    elif(json["action"] == "start" ):
        app.sensorsManager.startGame()
    elif(json["action"] == "stop"):
        app.sensorsManager.stopGame()

def mqtt_on_connect_callback(client, userdata, flags, rc):
    """Callback for successfull connection to broker. 
    It is called again after reconnecting after connection drops."""
    print("Connected to MQTT broker with result code "+str(rc))
    client.subscribe("laser_maze/#")


def mqtt_message_callback(client, userdata, msg):
    """Reacts on every MQTT message received from broker."""
    topic = str(msg.topic)
    payload = msg.payload.decode("utf-8")
    
    # Send data to MQTT log webpage. 
    txData = {"topic": topic, "payload": payload}
    socketio.emit('mqttMessage', txData, room='mqttlog')
    
    app.sensorsManager.handle_mqtt_message(topic, payload)
    

def socketio_emit(room, messageName, messageData):
    socketio.emit(messageName, messageData, room=room)
    

def my_print(*args):
    """Print message which prints the row also to MQTT message log. 
    Usefull when stdout of this server is unavailable."""
    print(*args)
    msg = ""
    for i in args:
        msg += str(i)+" "
    socketio.emit('mqttMessage', {"topic": "log", "payload":msg}, room='mqttlog')
    print("EMIT ok")
    

if __name__ == "__main__":
    print("LaserMazeServer: "+VERSION)
    print("created by hemmond, 2020")
    
    app.sensorsManager = SensorsManager(socketio_emit)

    client = mqtt.Client()
    client.on_connect = mqtt_on_connect_callback
    client.on_message = mqtt_message_callback

    client.connect(MQTT_BROKER, 1883, 60)
    client.loop_start()
    
    # Hide GET and POST messages from console output
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    
    #TODO remove - for debugging purposes.
    #app.sensorsManager.addSensor("LM_S01")
    #app.sensorsManager.sensorBox("LM_S01").enable(4)
    
    try:
        #socketio.run(app, debug=True, use_reloader=False)
        #scheduler.start()
        socketio.run(app, debug=True, use_reloader=False, host='0.0.0.0')
    except (KeyboardInterrupt, SystemExit):
        #scheduler.shutdown()
        client.loop_stop()
