class SensorBox:
    """Represents sensor box device - device with
    Esp8266/esp32 to which sensors are connected."""
    
    SENSOR_COUNT = 4 # <- how many sensors can be connected to the sensor box
    
    def __init__(self, box_id, sensorsManager):
        """Constructor. 
        Parameter box_id is string with Box identificator."""
        self.box_id = box_id
        self.sensorsManager = sensorsManager
        
        # each field is info for one sensor. Sensors are indexed from 1!
        self.states = [0]*SensorBox.SENSOR_COUNT
        self.active = [False]*SensorBox.SENSOR_COUNT
        print("init box", box_id)
        
    
    def handle_value_change_message(self, sensor_id, value):
        """This message is called when MQTT message 
        for this sensor is received."""
        value = int(value)
        if(self.active[sensor_id] and int(self.states[sensor_id]) != value):
            #sensor state changed
            self.states[sensor_id] = value
            messageData = {"boxId": self.box_id, "sensorId": sensor_id+1, "value": value}
            self.sensorsManager.socketio_emit("game", "sensorTelemetry", messageData)
            
            if(value == 0):
                self.sensorsManager.lineTripped()
        return
    
    
    def getSensorsData(self):
        """Generates dict containing all data of this sensor node"""
        retVal = {"boxId": self.box_id}
        retVal["values"] = self.states.copy()
        retVal["active"] = self.active.copy()
        retVal["sensorCount"] = SensorBox.SENSOR_COUNT
        return retVal
    
    
    def enable(self, sensor):
        self.active[sensor-1] = True
    
    
    def disable(self, sensor):
        self.active[sensor-1] = False


class SensorsManager:
    #Todo implement START, STOP a pokud nepobezi hra, neposilaji se zpravy tripped. 
    
    def __init__(self, socketioEmitFunction):
        self.sensors = {}
        self.sensorKeys = [] #here are keys of added elements. Also here is order of sensorBoxes.
        self.socketioEmitFunction = socketioEmitFunction
        
        self.trippedTimes = 0
        self.gameRunning = False
    
    
    def getSensorsData(self):
        retList = []
        for boxId in self.sensorKeys:
            retList.append(self.sensors[boxId].getSensorsData())
        return retList
        
    
    def addSensor(self, box_id):
        print("adding sensor", box_id)
        self.sensors[box_id] = SensorBox(box_id, self)
        self.sensorKeys.append(box_id)  #TODO: make sure, the item is not duplicitly added here. Each item must be unique in this list.
        # here should be sorting if order in which items were added is not preferable sorting algorithm.
    
    
    def sensorBox(self, box_id):
        """returns SensorBox with box_id"""
        return self.sensors[box_id]
    
    
    def handle_mqtt_message(self, topic, payload):
        print("handle mqtt:", topic, payload)
        
        topic_parts = topic.split("/")
        box_id = topic_parts[2]
        sensor_id = int(topic_parts[3])-1 #sensors are indexed from 1
        self.sensors[box_id].handle_value_change_message(sensor_id, payload)
    
    
    def socketio_emit(self, *args, **kwargs):
        self.socketioEmitFunction(*args, **kwargs)
    
    
    def lineTripped(self):
        """This function is called when sensor loses laser beam and acts accordingly. """
        if(self.gameRunning):
            self.trippedTimes +=1
            messageData = {"action": "tripped", "trippedTimes": self.trippedTimes}
            self.socketio_emit("game", "gameData", messageData)
    
    
    def resetTrippedCounter(self):
        self.trippedTimes = 0
        messageData = {"action": "reset", "trippedTimes": self.trippedTimes}
        self.socketio_emit("game", "gameData", messageData)
    
    
    def getTrippedTimes(self):
        return self.trippedTimes
    
    
    def getGameState(self):
        return self.gameRunning
    
    
    def startGame(self):
        self.gameRunning = True
        self._gameStateChanged()
    
    
    def stopGame(self):
        self.gameRunning = False
        self._gameStateChanged()
    
    
    def _gameStateChanged(self):
        messageData = {"action": "gameStateChanged", "gameRunning": self.gameRunning}
        self.socketio_emit("game", "gameData", messageData)
    
    